﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class TransitionToMainMenu : StateMachineBehaviour {

	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		LoadMainMenu();
	}

	private static void LoadMainMenu()
	{
		SceneManager.LoadScene(1);
	}
}
