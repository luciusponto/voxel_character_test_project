﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SplashInput : MonoBehaviour {

	// Update is called once per frame
	void Update () {
		if (AnythingPressed())
		{
			LoadMainMenu();
		}
	}

	private static bool AnythingPressed()
	{
		return Input.anyKey;
	}

	private static void LoadMainMenu()
	{
		SceneManager.LoadScene(1);
	}
}
