﻿using UnityEngine;

public abstract class Health : MonoBehaviour
{

    [SerializeField]
    protected float initialHealth;
    protected float currentHealth;

    protected void Start()
    {
        currentHealth = initialHealth;
    }

    public void Restore(float amount)
    {
        AdjustCurrentHealth(amount);
    }

    private void AdjustCurrentHealth(float amount)
    {
        currentHealth = Mathf.Clamp(currentHealth + amount, 0, initialHealth);
        OnHealthUpdate();
        if (currentHealth <= 0)
        {
            OnZeroHealth();
        }
    }

    public void Damage(float amount)
    {
        AdjustCurrentHealth(-amount);
    }

    public abstract void OnHealthUpdate();

    public abstract void OnZeroHealth();

}
