﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SelectNextChild))]
public class ObjectBuilderEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        
        SelectNextChild myScript = (SelectNextChild)target;
        if(GUILayout.Button("Next"))
        {
            myScript.SelectNext();
        }
        if(GUILayout.Button("Previous"))
        {
            myScript.SelectPrevious();
        }
        if(GUILayout.Button("Reset"))
        {
            myScript.Initialize();
        }
    }
}
