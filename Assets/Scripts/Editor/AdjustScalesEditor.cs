﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(AdjustScales))]
public class AdjustScalesEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        
        AdjustScales myScript = (AdjustScales)target;
        if(GUILayout.Button("Adjust"))
        {
            myScript.Adjust();
        }
   }
}
