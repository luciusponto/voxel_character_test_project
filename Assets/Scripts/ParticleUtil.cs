﻿using UnityEngine;

public class ParticleUtil
{
    public static float GetEmissionRateOverTime(ParticleSystem particleSystem)
    {
        return particleSystem.emission.rateOverTime.constant;
    }

    public static void SetEmissionRateOverTime(ParticleSystem particleSystem, float rate)
    {
        var emission = particleSystem.emission;
        var rateOverTime = particleSystem.emission.rateOverTime;
        rateOverTime.constant = rate;
        emission.rateOverTime = rateOverTime;
    }
}