﻿using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class SmokePuffs : MonoBehaviour
{
	private ParticleSystem _particleSystem;
	public int period = 4;
	private float timeSinceStartOfPeriod = 0f;
	[Range(0, 1f)]
	public float activeFractionOfPeriod = 1f;
	private float cutoffTime;
	private bool particlesOn = true;

	private float configuredRateOverTime;

	// Use this for initialization
	void Start ()
	{
		_particleSystem = GetComponent<ParticleSystem>();
		configuredRateOverTime = ParticleUtil.GetEmissionRateOverTime(_particleSystem);

	}

	public void SetActiveFractionOfPeriod(float fraction)
	{
		if (fraction < 0)
		{
			fraction = 0;
			LogValidationWarning();
		} else if (fraction > 1)
		{
			fraction = 1f;
			LogValidationWarning();
		}
		activeFractionOfPeriod = fraction;
	}
	
	private static void LogValidationWarning()
	{
		Debug.LogWarning("Fraction should be between 0 and 1");
	}

	public void SetSize()
	{
		
	}

	// Update is called once per frame
	void Update ()
	{
		timeSinceStartOfPeriod += Time.deltaTime;
		if (timeSinceStartOfPeriod > period)
		{
			timeSinceStartOfPeriod = 0;
			ParticleUtil.SetEmissionRateOverTime(_particleSystem, configuredRateOverTime);
			particlesOn = true;

		} else if (particlesOn && timeSinceStartOfPeriod > period * activeFractionOfPeriod)
		{
			particlesOn = false;
			ParticleUtil.SetEmissionRateOverTime(_particleSystem, 0f);
		}
	}
}