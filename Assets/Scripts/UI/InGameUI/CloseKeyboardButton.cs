﻿using UnityEngine;

public class CloseKeyboardButton : MonoBehaviour
{

	public RectTransform keyboardPanelRectTransform;
	public RectTransform keyboardCanvasRectTransform;
	private Rect currentDimensions;
	private RectTransform buttonRectTransform;
	
	// Use this for initialization
	void Start ()
	{
		var keyboardPanel = gameObject.transform.root.GetComponentInChildren<KeyboardPanel>();
		keyboardPanelRectTransform = keyboardPanel.GetComponent<RectTransform>();
		currentDimensions = keyboardPanelRectTransform.rect;
		buttonRectTransform = GetComponent<RectTransform>();
	}
	
	// Update is called once per frame
	void Update () {
		if (!currentDimensions.Equals(keyboardPanelRectTransform.rect))
		{
			SetPosition();
			currentDimensions = keyboardPanelRectTransform.rect;
		}
	}

	private void SetPosition()
	{
		var size = buttonRectTransform.rect.size.normalized;
		var largestDimension = Mathf.Max(size.x, size.y);
		buttonRectTransform.anchorMin = new Vector2(1 - largestDimension, keyboardPanelRectTransform.anchorMax.y);
		buttonRectTransform.anchorMax = new Vector2(1, keyboardPanelRectTransform.anchorMax.y + largestDimension);
		buttonRectTransform.offsetMax = Vector2.zero;
		buttonRectTransform.offsetMin = Vector2.zero;
	}
}
