﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGameUI : MonoBehaviour {

    public void Quit()
    {
        MenuManager.Show(Constants.CONFIRM_QUIT_TO_MAIN_MENU);
    }

}
