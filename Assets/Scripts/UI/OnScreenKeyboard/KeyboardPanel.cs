﻿public class KeyboardPanel : AnimatedUI, IWordGuessingListener
{
	public void OnStartGuessing(WordGuessingStatus status)
	{
		Show();
	}

	public void OnFinishedGuessing(WordGuessingStatus status)
	{
		Hide();
	}

	public void OnUpdate(WordGuessingStatus status)
	{
		// do nothing
	}

	protected override void Start()
	{
		base.Start();
		WordGuessingManager.Instance.RegisterListener(this);
	}

	private void OnDestroy()
	{
		WordGuessingManager.Instance.RemoveListener(this);
	}
	
}
