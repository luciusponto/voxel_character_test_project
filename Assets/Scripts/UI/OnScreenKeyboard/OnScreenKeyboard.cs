﻿using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class OnScreenKeyboard : MonoBehaviour
{

	public Camera keyboardCamera;
	public Rect dimensions;
	public bool resizeCameraViewport;
	public GameObject keyPrefab;
	private RectTransform keyboardRectTransform;
	public float minimumMargin = 0.05f;
	public float keySpacing = 0.01f;
	[Range(0, 10f)]
	public float maxKeyWidthToHeight = 1.5f;
	public float portraitHeightRatio = 0.25f;
	public Color panelColor = new Color(0, 0, 0, 50);
	public Color letterColor = Color.magenta;
	public Color normalButtonColor = new Color(0, 0, 0, 50);
	[Range(0,1)]
	public float disabledButtonAlpha = 0.5f;
	[Range(0,1)]
	public float highlightedButtonAlpha = 0.9f;

	// TODO: load this from file
	private static string QWERTY_LAYOUT = "qwertyuiop" + LAYOUT_SEPARATOR + "asdfghjkl" + LAYOUT_SEPARATOR + "zxcvbnm";
	private const char LAYOUT_SEPARATOR = ';';
	
	[SerializeField]
	private bool upperCase = true;

	private List<GameObject> keys;
	private Rect currentSize;
	public float fontSizeRatio = 0.5f;
	[Range(-1,1)]
	public float highlightedButtonExtraBrightness = 0.2f;
	[Range(-1,1)]
	public float disabledButtonExtraBrightness = -0.1f;

	private List<char> inputBuffer;
	public static OnScreenKeyboard Instance;

	private void SetupCamera(bool isEnabled)
	{
		keyboardCamera.rect = dimensions;
		Camera mainCamera = Camera.main;
		if (mainCamera)
		{
			Rect mainCameraRect = Camera.main.rect;
			if (isEnabled)
			{
				mainCameraRect.height = 1 - dimensions.height;
				mainCameraRect.y = 1 - mainCameraRect.height;
			}
			else
			{
				mainCameraRect.height = 1;
				mainCameraRect.y = 0;
			}
			Camera.main.rect = mainCameraRect;
		}
	}


	private void OnEnable()
	{
		if (resizeCameraViewport)
		{
			SetupCamera(true);
		}
	}

	private void OnDisable()
	{
		if (resizeCameraViewport)
		{
			SetupCamera(false);
		}
	}

	// Use this for initialization
	void Start ()
	{
		if (Instance == null)
		{
			inputBuffer = new List<char>(20);
			var keyboardPanel = gameObject.GetComponentInChildren<KeyboardPanel>();
			var panelImage = keyboardPanel.GetComponent<Image>();
			panelImage.color = panelColor;
			keyboardRectTransform = keyboardPanel.GetComponentInChildren<KeyboardPanel>().GetComponent<RectTransform>();
			DontDestroyOnLoad(gameObject);
			string[] layout = GetCurrentKeyboardLayout();
			AddKeys(layout);
			currentSize = keyboardRectTransform.rect;
			SetupLayout(layout);
			Instance = this;
		}
		else
		{
			Destroy(gameObject);
		}
		
	}

	private void ResetKeys()
	{
		foreach (var key in keys)
		{
			var button = key.GetComponent<Button>();
			if (button != null)
			{
				button.interactable = true;
			}
		}
	}

	private void SetupLayout(string[] layout)
	{
		SetKeyboardSize();
		int maxColumnCount = GetMaxColumnCount(layout);
		int rowCount = layout.Length;
		float keyboardWidth = keyboardRectTransform.rect.width;
		float keyboardHeight = keyboardRectTransform.rect.height;
		float keyboardLargestDimension = Mathf.Max(keyboardWidth, keyboardHeight);
		float widthMargin = minimumMargin * keyboardLargestDimension / keyboardWidth;
		float heightMargin = minimumMargin * keyboardLargestDimension / keyboardHeight;
		// the key height will be constant when the screen is rotated, while the key width will adapt to screen width
		var keyHeightRatio = (1 - 2 * heightMargin - (rowCount - 1) * keySpacing) / rowCount;
		float keyWidthRatio = (1 - widthMargin * 2 - keySpacing * maxColumnCount) / maxColumnCount;
		float keyWidthPixels = keyWidthRatio * keyboardRectTransform.rect.width;
		float keyHeightPixels = keyHeightRatio * keyboardRectTransform.rect.height;
		if (keyWidthPixels > maxKeyWidthToHeight * keyHeightPixels)
		{
			keyWidthPixels = maxKeyWidthToHeight * keyHeightPixels;
			keyWidthRatio = keyWidthPixels / keyboardWidth;
		}
		Vector2 parentPosition = keyboardRectTransform.rect.position;

		float fontSize = fontSizeRatio * Mathf.Min(keyHeightPixels, keyWidthPixels);
		
		foreach (var key in keys)
		{
			var keyInfo = key.GetComponent<KeyboardKey>();
			var keyText = key.GetComponentInChildren<TextMeshProUGUI>();
			keyText.fontSize = fontSize;
			var rect = key.GetComponent<RectTransform>();
//			var centeringXOffset = (maxColumnCount - keyInfo.rowSize) * 0.5f * keyWidthRatio;
			var centeringXOffset =
				(1f - 2 * widthMargin - keyInfo.RowSize * keyWidthRatio - (keyInfo.RowSize - 1) * keySpacing) * 0.5f;
			var widthPlusSpacing = (keyWidthRatio + keySpacing);
			rect.anchorMin = new Vector2(widthMargin + centeringXOffset + keyInfo.Column * widthPlusSpacing, heightMargin + (rowCount - 1 - keyInfo.Row) * (keyHeightRatio + keySpacing));
			rect.anchorMax = rect.anchorMin + new Vector2(keyWidthRatio, keyHeightRatio);
			rect.offsetMax = Vector2.zero;
			rect.offsetMin = Vector2.zero;
		}
		 

	}

	private void SetKeyboardSize()
	{
		var canvas = GetComponentInChildren<Canvas>();
		var canvasRectTransform = canvas.GetComponent<RectTransform>();
		float heightRatio = portraitHeightRatio * Mathf.Max(canvasRectTransform.rect.height, canvasRectTransform.rect.width) / canvasRectTransform.rect.height;
		keyboardRectTransform.anchorMin = Vector2.zero;
		keyboardRectTransform.anchorMax = new Vector2(1f, heightRatio);
		keyboardRectTransform.offsetMax = Vector2.zero;
		keyboardRectTransform.offsetMin = Vector2.zero;
	}

	private int GetMaxColumnCount(string[] layout)
	{
		int value = 0;
		foreach (var row in layout)
		{
			value = Mathf.Max(value, row.Length);
		}
		return value;
	}

	private void AddKeys(string[] layout)
	{
		keys = new List<GameObject>(GetKeyCount(layout));
//		letterColor = AddBrightness(letterColor, 1f); // make it maximum brightness
		Color disabledButtonColor = AddBrightness(letterColor, disabledButtonExtraBrightness);
		disabledButtonColor.a = disabledButtonAlpha;
		Color highlightedButtonColor = AddBrightness(normalButtonColor, highlightedButtonExtraBrightness);
		highlightedButtonColor.a = highlightedButtonAlpha;
		short rowIndex = 0;
		foreach (var row in layout)
		{
			short columnIndex = 0;
			foreach (var keyValue in row)
			{
				var key = Instantiate(keyPrefab, keyboardRectTransform.transform);
				var button = key.GetComponent<Button>();
				var colorBlock = button.colors;
				colorBlock.normalColor = normalButtonColor;
				colorBlock.highlightedColor = highlightedButtonColor;
				colorBlock.disabledColor = disabledButtonColor;
				colorBlock.pressedColor = disabledButtonColor;
				button.colors = colorBlock;
				
				var text = key.GetComponentInChildren<TextMeshProUGUI>();
				if (text != null)
				{
					text.text = upperCase ? keyValue.ToString().ToUpper() : keyValue.ToString().ToLower();
					text.color = letterColor;
				}
				var keyScript = key.GetComponent<KeyboardKey>();
				keyScript.SetKeyboard(this);
				keyScript.Row = rowIndex;
				keyScript.Column = columnIndex;
				keyScript.RowSize = (short) row.Length;
				keyScript.KeyValue = keyValue;
				keys.Add(key);
				columnIndex++;
			}
			rowIndex++;
		}
	}

	private Color AddBrightness(Color inputColor, float brightness)
	{
		float h = 0;
		float s = 0;
		float v = 0;
		Color.RGBToHSV(inputColor, out h, out s, out v);
		return Color.HSVToRGB(h, s, Mathf.Clamp(v + brightness, -1, 1));
	}

	private int GetKeyCount(string[] layout)
	{
		int count = 0;
		foreach (var row in layout)
		{
			count += row.Length;
		}
		return count;
	}

	// Update is called once per frame
	void Update () {
		if (!keyboardRectTransform.rect.Equals(currentSize))
		{
			SetupLayout(GetCurrentKeyboardLayout());
			currentSize = keyboardRectTransform.rect;
		}
	}


	private string[] GetCurrentKeyboardLayout()
	{
		return QWERTY_LAYOUT.Split(LAYOUT_SEPARATOR);
	}

	public string GetInputString()
	{
		StringBuilder stringBuilder = new StringBuilder(inputBuffer.Count);
		foreach (char letter in inputBuffer)
		{
			stringBuilder.Append(letter);
		}
		inputBuffer.Clear();
		return stringBuilder.ToString();
	}

	public void AddToInput(char keyValue)
	{
		inputBuffer.Add(keyValue);
	}
}

