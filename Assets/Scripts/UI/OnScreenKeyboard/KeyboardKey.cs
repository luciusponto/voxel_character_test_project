﻿using UnityEngine;
using UnityEngine.UI;

public class KeyboardKey : MonoBehaviour
{

    private char keyValue;
    private short row;
    private short column;
    private short rowSize;
    private OnScreenKeyboard keyboard;

    public void OnKeyPressed()
    {
        keyboard.AddToInput(keyValue);
    }
    
    public void SetKeyboard(OnScreenKeyboard onScreenKeyboard)
    {
        keyboard = onScreenKeyboard;
    }
    

    public char KeyValue
    {
        get { return keyValue; }
        set { keyValue = value; }
    }

    public short Row
    {
        get { return row; }
        set { row = value; }
    }

    public short Column
    {
        get { return column; }
        set { column = value; }
    }

    public short RowSize
    {
        get { return rowSize; }
        set { rowSize = value; }
    }
}
