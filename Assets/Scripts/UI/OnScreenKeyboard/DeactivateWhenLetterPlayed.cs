﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeactivateWhenLetterPlayed : MonoBehaviour, IWordGuessingListener
{

	private KeyboardKey key;
	private Button button;
	[SerializeField]
	private Color wrongLetterColor;
	[SerializeField]
	private Color correctLetterColor;

	private void Start()
	{
		key = GetComponent<KeyboardKey>();
		button = GetComponent<Button>();
		WordGuessingManager.Instance.RegisterListener(this);
	}

	public void OnStartGuessing(WordGuessingStatus status)
	{
		button.interactable = true;
	}

	public void OnFinishedGuessing(WordGuessingStatus status)
	{
		button.interactable = true;
	}

	public void OnUpdate(WordGuessingStatus status)
	{
		if (status.LatestPlayedLetter.Equals(key.KeyValue))
		{
			button.interactable = false;
			var colors = button.colors;
			colors.disabledColor = status.LatestLetterSuccessful ? correctLetterColor : wrongLetterColor;
			button.colors = colors;
		}
	}

	private void OnDestroy()
	{
		WordGuessingManager.Instance.RemoveListener(this);
	}
}
