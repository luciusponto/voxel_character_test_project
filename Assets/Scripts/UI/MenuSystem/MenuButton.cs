﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MenuButton : MonoBehaviour, IPointerEnterHandler, ISelectHandler, IDeselectHandler
{
	private Button button;
	private Menu menu;
	private Animator animator;
	private bool selectOnEnable;
	
	// Use this for initialization
	void Start ()
	{
		button = GetComponent<Button>();
		menu = GetComponentInParent<Menu>();
		animator = GetComponent<Animator>();
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		Select();
	}

	public void Select()
	{
		button.Select();
		OnSelect(null);
	}

	public void SelectOnEnable()
	{
		selectOnEnable = true;
	}

	public void OnSelect(BaseEventData eventData)
	{
		if (menu != null)
		{
			menu.lastSelectedButton = this;
		}
		animator.SetTrigger("Selected");
	}

	private void OnEnable()
	{
		if (selectOnEnable)
		{
			Select();
		}
		selectOnEnable = false;
	}

	public void OnDeselect(BaseEventData eventData)
	{
		animator.SetTrigger("Deselected");
	}
}
