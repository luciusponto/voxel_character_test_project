﻿using UnityEngine;

public class Menu : MonoBehaviour
{
	[SerializeField, Tooltip("Should be true only if this menu is expected to be open and closed very frequently")]
	private bool keepInMemoryWhenClosed = false;
	[SerializeField]
	private bool saveStateOnExit = true;
	[SerializeField, Tooltip("If true, any menus open underneath will be disabled while this menu is open.")]
	private bool hidePreviousMenu = false;
	[HideInInspector]
	public MenuButton lastSelectedButton;

	public virtual void Close()
	{
		if (saveStateOnExit)
		{
			SaveState();
		}
		if (keepInMemoryWhenClosed)
		{
			gameObject.SetActive(false);
		}
		else
		{
			Destroy(gameObject);
		}
	}
	
	public bool KeepInMemoryWhenClosed()
	{
		return keepInMemoryWhenClosed;
	}

	public virtual void Open()
	{
		// do nothing
	}
	
	private void SaveState()
	{
		Debug.Log("Menu state saving not yet implemented");
	}

	// Use this for initialization
//	void Start () {
//		RegisterWithManager();
//	}
	
	public virtual void OnBackPressed()
	{
		// do nothing
	}

//	void RegisterWithManager()
//	{
//		MenuManagerNew.Register(gameObject.name, this);
//	}

//	void OnDestroy()
//	{
//		MenuManagerNew.Unregister(gameObject.name);
//	}

	public bool HidePreviousMenu()
	{
		return hidePreviousMenu;
	}

	protected void OnEnable()
	{
		if (lastSelectedButton != null)
		{
			lastSelectedButton.SelectOnEnable();
		}
	}
}
