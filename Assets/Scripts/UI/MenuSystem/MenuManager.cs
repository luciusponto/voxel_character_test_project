﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class MenuManager : MonoBehaviour
{

	public static MenuManager Instance;
	
	[SerializeField]
	private Menu[] menuPrefabs;
	private Dictionary<string, Menu> prefabsDictionary;
	private Dictionary<string, Menu> instancedMenus;
	private Stack<Menu> menusOnDisplay;
	
	public static void Show(string menuName)
	{
		Menu menu = Instance.GetMenu(menuName);
		if (menu != null)
		{
			menu.gameObject.SetActive(true);
			menu.Open();
			if (Instance.menusOnDisplay.Count > 0)
			{
				Canvas newMenuCanvas = menu.gameObject.GetComponent<Canvas>();
				Canvas currentMenuCanvas = Instance.menusOnDisplay.Peek().GetComponent<Canvas>();
				if (currentMenuCanvas != null && newMenuCanvas != null)
				{
					newMenuCanvas.sortingOrder = currentMenuCanvas.sortingOrder + 1;
				}
				if (menu.HidePreviousMenu())
				{
					foreach (var currentlyOpenMenu in Instance.menusOnDisplay)
					{
						currentlyOpenMenu.gameObject.SetActive(false);
						if (currentlyOpenMenu.HidePreviousMenu())
						{
							break; // previous ones will already be hidden - no need to continue loop
						}
					}
				}
			}
			Instance.menusOnDisplay.Push(menu);
		}
	}

	private void Register(string name, Menu script)
	{
		if (!instancedMenus.ContainsKey(name))
		{
			instancedMenus.Add(name, script);
		}
		else
		{
			Debug.LogWarning("Tried to register UI screen \"" + name + "\" twice. Ignored.");
		}
	}
	
	private void Unregister(string name)
	{
		if (!instancedMenus.Remove(name))
		{
			Debug.LogWarning("Tried to unregister UI screen \"" + name + "\" but it was not found. " +
			                 "current registered menus: " + KeysToString(instancedMenus));
		}
	}

	private Menu GetMenu(string menuName)
	{
		Menu menu = null;
		if (instancedMenus.TryGetValue(menuName, out menu))
		{
			return menu;
		}
		else
		{
			Menu prefab;
			if (prefabsDictionary.TryGetValue(menuName, out prefab))
			{
				menu = Instantiate(prefab, gameObject.transform);
				menu.gameObject.name = menuName;
				if (menu != null)
				{
					Register(menuName, menu);
				}
			}
			else
			{
				Debug.LogError("Menu \"" + menuName + "\" could not be instantiated or retrieved from memory." +
				               "Please check that the menu prefab matches exactly one of the prefabs referenced in the menu manager." +
				               "Prefabs currently referenced in menu manager: " + KeysToString(prefabsDictionary));
			}
		}
		return menu;
	}

	private static string KeysToString<K, V>(Dictionary<K, V> dictionary)
	{
		StringBuilder stringBuilder = new StringBuilder();
		foreach (var prefabName in dictionary.Keys)
		{
			stringBuilder.Append(prefabName).Append("\n");
		}
		return stringBuilder.ToString();

	}
	
	public static void Hide(string menuName)
	{
		Menu menu = Instance.menusOnDisplay.Peek();
		if (menu != null && menu.name.Equals(menuName))
		{
			Instance.menusOnDisplay.Pop();
			if (menu.HidePreviousMenu())
			{
				foreach (var previousMenu in Instance.menusOnDisplay)
				{
					previousMenu.gameObject.SetActive(true);
					if (previousMenu.HidePreviousMenu())
					{
						break;
					}
				}
			}
			if (!menu.KeepInMemoryWhenClosed())
			{
				Instance.Unregister(menuName);
			}
			menu.Close();
		}
		else
		{
			Debug.LogError("Tried to close menu which is not at the top of the stack. Ignored.");
		}
	}

	void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
			instancedMenus = new Dictionary<string, Menu>(menuPrefabs.Length);
			menusOnDisplay = new Stack<Menu>(menuPrefabs.Length);
			PopulatePrefabsDictionary();
			DontDestroyOnLoad(gameObject);
		}
		else
		{
			// only allow one instance
			Destroy(gameObject);
		}
	}

	private void PopulatePrefabsDictionary()
	{
		prefabsDictionary = new Dictionary<string, Menu>(menuPrefabs.Length);
		foreach (var menuPrefab in menuPrefabs)
		{
			prefabsDictionary.Add(menuPrefab.gameObject.name, menuPrefab);
		}
	}

	// Update is called once per frame
	void Update () {
		
		// Escape is also the key code for the Android "back" key
		if (Input.GetKeyDown(KeyCode.Escape) && Instance.menusOnDisplay.Count > 0)
		{
			Instance.menusOnDisplay.Peek().OnBackPressed();
		}
	}
}
