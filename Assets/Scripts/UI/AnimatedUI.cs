﻿using UnityEngine;

public class AnimatedUI : MonoBehaviour
{

	private RectTransform rectTransform;
	private Animator animator;
	
	public float GetHeightRatio()
	{
		if (rectTransform != null)
		{
			return rectTransform.anchorMax.y - rectTransform.anchorMin.y;
		}
		return 1f;
	}

	// Use this for initialization
	protected virtual void Start ()
	{
		rectTransform = GetComponent<RectTransform>();
		animator = GetComponent<Animator>();
	}
	
	public void Toggle()
	{
		animator.SetBool("IsVisible", !animator.GetBool("IsVisible"));
	}
	
	public void Show()
	{
		animator.SetBool("IsVisible", true);			
	}
	
	public void Hide()
	{
		animator.SetBool("IsVisible", false);			
	}
	
}
