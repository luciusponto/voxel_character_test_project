﻿using System.Text;
using TMPro;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class WordGuessingUI : AnimatedUI, IWordGuessingListener
{

	[SerializeField]
	private TextMeshProUGUI wordCategoryText;
	[SerializeField]
	private TextMeshProUGUI mistakesText;
	[SerializeField]
	private TextMeshProUGUI guessedWordText;
	[SerializeField] private bool guessedWordAllCaps;
	private StringBuilder builder;
	[SerializeField] private Gradient mistakesGradient;
	[SerializeField] private char incognitoCharacter = '-';
	[SerializeField] private Animator mistakesAnimator;
	[SerializeField] private Animator guessedWordAnimator;
	

	public void OnStartGuessing(WordGuessingStatus status)
	{
		StopAllCoroutines();
		Show();
		builder.Clear();
		wordCategoryText.text = status.Word.Category.name;
		mistakesAnimator.SetBool("OneMistakeLeft", false);
		SetGuessedWordText(status);
		SetMistakesCounter(status);
	}

	private void SetMistakesCounter(WordGuessingStatus status)
	{
		builder.Clear();
		builder.Append(status.Mistakes).Append("/").Append(status.MaxMistakes);
		mistakesText.text = builder.ToString();
		mistakesText.color = GetColor(status.Mistakes, status.MaxMistakes);
		if (OneMistakeLeft(status))
		{
			mistakesAnimator.SetBool("OneMistakeLeft", true);
		}
		else
		{
			mistakesAnimator.SetTrigger("WrongLetterPlayed");
		}
	}

	public void OnFinishedGuessing(WordGuessingStatus status)
	{
		Hide();
	}

	public void OnUpdate(WordGuessingStatus status)
	{
		if (status.LatestLetterSuccessful)
		{
			SetGuessedWordText(status);
			guessedWordAnimator.SetTrigger("CorrectLetterPlayed");
		}
		else
		{
			SetMistakesCounter(status);
		}
	}

	private void SetGuessedWordText(WordGuessingStatus status)
	{
		var guessedSoFar = guessedWordAllCaps ? status.GuessedSoFar.ToUpper() : status.GuessedSoFar;
		SetGuessedWordText(guessedSoFar);
	}

	private void SetGuessedWordText(string guessedSoFar)
	{
		guessedWordText.text = guessedSoFar.Replace(WordGuessingManager.IncognitoCharacter, incognitoCharacter);
	}

	private bool OneMistakeLeft(WordGuessingStatus status)
	{
		return status.Mistakes == status.MaxMistakes - 1;
	}

	private Color GetColor(int mistakes, int maxMistakes)
	{
		float point = (float) Mathf.Min(mistakes, maxMistakes - 1) / (maxMistakes - 1);
		return mistakesGradient.Evaluate(point);
	}

	protected override void Start()
	{
		base.Start();
		builder = new StringBuilder();
		WordGuessingManager.Instance.RegisterListener(this);
	}

	private void OnDestroy()
	{
		WordGuessingManager.Instance.RemoveListener(this);
	}
}
