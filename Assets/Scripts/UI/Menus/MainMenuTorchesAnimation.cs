﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MainMenuTorchesAnimation : MonoBehaviour, ISelectHandler
{

//	[HideInInspector]
	public Animator environmentAnimator;
	private String triggerName;
	
	private void Start()
	{
		triggerName = gameObject.name;
	}

	private void AnimateSelected()
	{
		environmentAnimator.SetTrigger(triggerName);
	}

	public void OnSelect(BaseEventData eventData)
	{
		AnimateSelected();
	}

}
