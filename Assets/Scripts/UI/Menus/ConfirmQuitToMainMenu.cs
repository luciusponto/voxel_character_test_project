﻿public class ConfirmQuitToMainMenu : SubMenu {

    public void OnYesClicked()
    {
        GameManager.GetInstance().QuitToMainMenu();
        MenuManager.Hide(gameObject.name);
    }

    public void OnNoClicked()
    {
        MenuManager.Hide(gameObject.name);
    }
    
}
