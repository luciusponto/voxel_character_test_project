﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MainMenu : Menu
{

	public void Play(MenuButton callerButton)
	{
		lastSelectedButton = callerButton;
		GameManager.GetInstance().StartGame();
		MenuManager.Hide(Constants.MAIN_MENU);
	}

	public void Quit(MenuButton callerButton)
	{
		lastSelectedButton = callerButton;
		GameManager.GetInstance().QuitGame();
	}

	public void Options(MenuButton callerButton)
	{
		lastSelectedButton = callerButton;
		MenuManager.Show(Constants.OPTIONS_MENU);
	}

	public void OnEnable()
	{
		base.OnEnable();
		var buttons = GetComponentsInChildren<Button>();
		SetUpNavigation(buttons);
		var torchScripts = GetComponentsInChildren<MainMenuTorchesAnimation>();
		SetUpTorches(torchScripts);
	}

	private void SetUpNavigation(Button[] buttons)
	{
		EventSystem.current.firstSelectedGameObject = buttons[0].gameObject;
	}

	private void SetUpTorches(MainMenuTorchesAnimation[] torchScripts)
	{
		var environment = GameObject.Find("Environment");
		var environmentAnimator = environment.GetComponent<Animator>();
		foreach (var torchScript in torchScripts)
		{
			torchScript.environmentAnimator = environmentAnimator;
		}
	}
}
