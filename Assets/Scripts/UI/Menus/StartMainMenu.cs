﻿using UnityEngine;

public class StartMainMenu : MonoBehaviour {

	// Use this for initialization
	void Start () {
		MenuManager.Show(Constants.MAIN_MENU);
	}
}
