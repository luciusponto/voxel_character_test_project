﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AudioManager : MonoBehaviour
{

	public static AudioManager Instance;
	private AudioSource musicSource;
	private AudioSource ambientSource;
	private AudioSource effectSource;
	[SerializeField] private AudioClip[] musicTracksPerScene;
	[SerializeField] private AudioClip[] ambientTracksPerScene;
	public float lowPitchRange = 0.95f;
	public float highPitchRange = 1.05f;
	[SerializeField]
	private float musicFadeOutTime;

	private void Awake()
	{
		// Singleton
		if (Instance == null)
		{
			Instance = this;
			musicSource = GetComponents<AudioSource>()[0];
			ambientSource = GetComponents<AudioSource>()[1];
			effectSource = GetComponents<AudioSource>()[2];
			DontDestroyOnLoad(this);
		}
		else
		{
			Destroy(gameObject);
		}
	}

	private void PlayMusicAndAmbientForScene()
	{
		StartCoroutine(SmoothReplaceTrack(musicSource, GetTrackForScene(musicTracksPerScene)));
		StartCoroutine(SmoothReplaceTrack(ambientSource, GetTrackForScene(ambientTracksPerScene)));		
	}

	private void OnEnable()
	{
		SceneManager.sceneLoaded += OnSceneLoaded;
	}

	private void OnDisable()
	{
		SceneManager.sceneLoaded -= OnSceneLoaded;
	}

	private void OnSceneLoaded(Scene scene, LoadSceneMode loadSceneMode)
	{
		PlayMusicAndAmbientForScene();
	}

	private IEnumerator SmoothReplaceTrack(AudioSource audioSource, AudioClip clip)
	{
		float originalVolume = audioSource.volume;
		while (audioSource.volume > 0)
		{
			audioSource.volume -= originalVolume * Time.deltaTime / musicFadeOutTime;
			yield return null;
		}
		audioSource.Stop();
		audioSource.volume = originalVolume;
		if (clip != null)
		{
			audioSource.clip = clip;
			audioSource.Play();
		}
	}

	private AudioClip GetTrackForScene(AudioClip[] tracksPerScene)
	{
		return GetTrackForScene(tracksPerScene, SceneManager.GetActiveScene().buildIndex);
	}

	private AudioClip GetTrackForScene(AudioClip[] tracksPerScene, int sceneIndex)
	{
		var clipIndex = sceneIndex;
		if (clipIndex > tracksPerScene.Length - 1)
		{
			return null;
		}
		return tracksPerScene[clipIndex];
	}

	public static void PlaySound(AudioSource source, AudioClip clip)
	{
		PlaySound(source, 1f, clip);
	}

	public static void PlaySound(AudioSource source, float pitch, AudioClip clip)
	{
		PlaySound(source, pitch, clip, false);
	}

	public static void PlaySound(AudioSource source, float pitch, AudioClip clip, bool loop)
	{
		if (clip != null)
		{
			source.loop = loop;
			source.pitch = pitch;
			source.clip = clip;
			source.Play();
		}
	}

	public static void RandomizeSound(AudioSource source, float lowPitch, float highPitch, bool loop, params AudioClip[] clips)
	{
		if (clips != null && clips.Length > 0)
		{
			var pitch = Random.Range(lowPitch, highPitch);
			var clip = clips[Random.Range(0, clips.Length)];
			PlaySound(source, pitch, clip, loop);
		}
		else
		{
			Debug.LogWarning("Trying to play random audio clip from null/empty array");
		}
	}

}
