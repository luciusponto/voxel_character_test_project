﻿using UnityEngine;

[CreateAssetMenu(fileName = "SimpleAudioEvent", menuName = "AudioEvent/Simple")]
public class SimpleAudioEvent : AudioEvent
{
    public AdjustableAudioClip clip;
    
    public override void Play(AudioSource source)
    {
        source.volume = clip.Volume;
        source.pitch = clip.Pitch;
        source.clip = clip.Clip;
        source.Play();
    }
}
