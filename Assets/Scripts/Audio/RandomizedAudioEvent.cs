﻿using UnityEngine;

[CreateAssetMenu(fileName = "RandomizedAudioEvent", menuName = "AudioEvent/Randomized")]
public class RandomizedAudioEvent : AudioEvent
{
    public AudioClip[] clips;
    [MinMaxRange(0,2)]
    public RangedFloat volumeRange = new RangedFloat(1f, 1f);
    [MinMaxRange(0,2)]
    public RangedFloat pitchRange = new RangedFloat(0.95f, 1.05f);
    
    public override void Play(AudioSource source)
    {
        if (clips.Length == 0)
        {
            Debug.LogWarning("No audio clips assigned to AudioEvent " + this.name);
            return;
        }
        source.clip = clips[Random.Range(0, clips.Length)];
        source.volume = Random.Range(volumeRange.minValue, volumeRange.maxValue);
        source.pitch = Random.Range(pitchRange.minValue, pitchRange.maxValue);
        source.Play();
    }
}