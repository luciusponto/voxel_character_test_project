﻿using System;
using UnityEngine;

[Serializable]
public class AdjustableAudioClip
{
    public AudioClip Clip;
    [Range(0f,1f)] public float Volume;
    [Range(0f,1f)] public float Pitch;
    [Range(0f,1f)] public float ChanceOfPlaying;
}
