﻿using UnityEngine;

public class FootstepSounds : MonoBehaviour
{

    public AudioSource AudioSource;
    
    [SerializeField] private AudioEvent footStepSound;

    public void WalkContact()
    {
        footStepSound.Play(AudioSource);
    }

    public void RunContact()
    {
        WalkContact();
    }
    

}
