﻿using UnityEngine;

public class EquipmentAttachmentPoints : MonoBehaviour
{

	public Transform rightHand;
	public Transform leftHand;
	public Transform head;
	public bool rightHanded = true;

	// Use this for initialization
	void Start ()
	{
//		leftHand = GetBone("mixamorig:LeftHand");
//		rightHand = GetBone("mixamorig:RightHand");
//		head = GetBone("mixamorig:Head");
	}

	private Transform GetBone(string boneName)
	{
		return gameObject.transform.Find(boneName);
	}

	public Transform GetSpellAttachmentPoint()
	{
		if (rightHanded)
		{
			return rightHand;
		}
		else
		{
			return leftHand;
		}
	}

	// Update is called once per frame
	void Update () {
		
	}
}
