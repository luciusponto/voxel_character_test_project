﻿using UnityEngine;

public class WizardHealth : Health
{

	private MagicShield _magicShield;

	private void Start()
	{
		base.Start();
		_magicShield = gameObject.transform.GetComponentInChildren<MagicShield>();
	}

	public override void OnHealthUpdate()
	{
		if (_magicShield)
		{
			_magicShield.UpdateHealth((int)(currentHealth * 100 / initialHealth));
		}
	}

	public override void OnZeroHealth()
	{
		Debug.Log("GameOver");
	}
}
