﻿using System.Collections.Generic;
using UnityEngine;

public class SelectNextChild : MonoBehaviour
{

	private List<Transform> children;
	public int childCount;
	private int currenlyEnabledChildIndex = -1;
	private string FRAME_PREFIX = "Model_Frame_";

	public void Initialize()
	{
		children = new List<Transform>(gameObject.transform.childCount);
		childCount = 0;
		currenlyEnabledChildIndex = -1;
		for (int i = 0; i < gameObject.transform.childCount; i++)
		{
			var child = transform.GetChild(i);
			if (child.name.StartsWith(FRAME_PREFIX))
			{
				children.Add(child);
				if (child.gameObject.activeSelf)
				{
					if (currenlyEnabledChildIndex == -1)
					{
						currenlyEnabledChildIndex = i;
					}
					else
					{
						child.gameObject.SetActive(false);
					}
				}
			}
			if (currenlyEnabledChildIndex == -1 && children.Count > 0)
			{
				currenlyEnabledChildIndex = 0;
				children[0].gameObject.SetActive(true);
			}
			childCount = children.Count;
		}
	}

	public void SelectNext()
	{
		if (children == null || childCount == 0)
		{
			Initialize();
		}
		if (childCount > 0)
		{
			var nextChildIndex = currenlyEnabledChildIndex + 1;
			if (nextChildIndex >= childCount)
			{
				nextChildIndex = 0;
			}
			children[currenlyEnabledChildIndex].gameObject.SetActive(false);
			children[nextChildIndex].gameObject.SetActive(true);
			currenlyEnabledChildIndex = nextChildIndex;
		}
		
	}

	public void SelectPrevious()
	{
		if (children == null || childCount == 0)
		{
			Initialize();
		}
		if (childCount > 0)
		{
			var nextChildIndex = currenlyEnabledChildIndex - 1;
			if (nextChildIndex < 0)
			{
				nextChildIndex = childCount - 1;
			}
			children[currenlyEnabledChildIndex].gameObject.SetActive(false);
			children[nextChildIndex].gameObject.SetActive(true);
			currenlyEnabledChildIndex = nextChildIndex;
		}
	}
}
