﻿using Lean.Touch;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(AnimateNavMeshAgent))]
[RequireComponent(typeof(SpellCasting))]
public class ClickToControl : MonoBehaviour
{
    private RaycastHit hitInfo = new RaycastHit();
    private NavMeshAgent agent;
    private Animator animator;
    private AnimateNavMeshAgent navMeshAnimation;
    private SpellCasting _spellCasting;
    public GameObject equippedSpell;
    private Spell spell;
    private static readonly string[] clickableLayers = {Constants.DEFAULT_LAYER};
    private LayerMask clickableLayerMask;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        navMeshAnimation = GetComponent<AnimateNavMeshAgent>();
        _spellCasting = GetComponent<SpellCasting>();
        clickableLayerMask = LayerMask.GetMask(clickableLayers);
    }

    void Update()
    {
        if (MouseOverUIElement())
        {
            return; // UI system has precedence
        }
        if (Input.GetMouseButtonDown(0))
        {
            if (IsTryingToCast())
            {
                return; // spell casting mechanism should take over
            }
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray.origin, ray.direction, out hitInfo, Constants.MAX_RAYCAST_DISTANCE, clickableLayerMask))
            {
                if (hitInfo.transform.tag.Equals(Constants.ENVIRONMENT_TAG))
                {
                    WalkTo(hitInfo.point);
                }
                else if (hitInfo.transform.root.tag.Equals(Constants.ENEMY_TAG))
                {
                    if (!IsTryingToCast())
                    {
                        Attack(hitInfo.transform.root.gameObject, hitInfo.collider);
                    }
                }
            }
        }
    }

    private bool MouseOverUIElement()
    {
        return LeanTouch.PointOverGui(Input.mousePosition);
    }

    private bool IsTryingToCast()
    {
        return animator.GetBool(Constants.TRYING_TO_CAST);
    }

    private void Attack(GameObject target, Collider collider)
    {
        _spellCasting.TryCasting(equippedSpell, target, collider);
    }

    private void WalkTo(Vector3 destination)
    {
        agent.destination = destination;
    }

}