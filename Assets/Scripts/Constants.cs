﻿public class Constants
{

   #region gameplay
   public const string ENVIRONMENT_TAG = "Environment";
   public const string PLAYER_TAG = "Player";
   public const string ENEMY_TAG = "Enemy";
   public static string ENEMY_WEAPON = "EnemyWeapon";
   public static string CLICK_COLLIDER = "ClickCollider";

   #endregion
   
   #region animation
   public const string TRYING_TO_CAST = "TryingToCast";
   public const string CASTING = "Casting";
   public const string TURN = "Turn";
   public const string SPEED = "Speed";
   public const string IN_WAKEUP_RANGE = "InWakeUpRange";
   public const string STAND_UP_ANIMATION = "StandUpAnimation";
   #endregion
    
   #region messages
   public static string TARGET_TRIGGER_HIT = "TargetTriggerHit";
   public static string IS_DEAD = "OnDeath";
   #endregion
   
   #region menus
   public static string MAIN_MENU = "MainMenuWithEnvironment";
   public static string OPTIONS_MENU = "OptionsMenu";
   public static string GRAPHICS_MENU = "GraphicsOptionsMenu";
   public static string CONFIRM_QUIT_TO_MAIN_MENU = "ConfirmQuitToMainMenu";
   public static float MAX_RAYCAST_DISTANCE = 100;
   public static string ENVIRONMENT_LAYER = "Environment";
   public static string DEFAULT_LAYER = "Default";

   #endregion
}
