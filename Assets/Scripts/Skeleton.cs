﻿using System.Collections.Generic;
using Lean;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(Health))]
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(NavMeshObstacle))]
[RequireComponent(typeof(AnimateNavMeshAgent))]
public class Skeleton : MonoBehaviour
{
    private SkeletonState state;
    private GameObject targetPlayer;
    private Animator _animator;
    private NavMeshAgent _navMeshAgent;
    private NavMeshObstacle _navMeshObstacle;
    private AnimateNavMeshAgent _animateNavMeshAgent;
    [SerializeField, Range(0,20)]
    private float playerDetectionRange = 10f;
    [SerializeField, Range(0,20)]
    private float playerChaseRange = 8f;
    [SerializeField, Range(0,20)]
    private float attackRange = 2f;
    [SerializeField, Range(0,20)]
    private float attackCooldownTime = 3f;
    [SerializeField, Range(0,2)]
    private float maxRandomExtraCooldownTime = 0.5f;
    [SerializeField, Range(0,2)]
    private float attackDamage = 1f;
    private float nextAttackTime;
    private List<GameObject> players;
    private float playerRadius = 0f;
    private float maxAttackErrorAngle = 15f;
    private Health playerHealth;
    private Health _health;
    const float DEFAULT_PLAYER_RADIUS = 0.5f;
    public Collider weaponCollider;
    private CollapseBones collapseBones;
    private Collider clickCollider;
    public GameObject eyes;
    private AudioSource audioSource;
    [SerializeField]
    private AudioEvent bonesCollapsingSound;
    [SerializeField]
    private AudioEvent wakingUpSound;

    // Use this for initialization
    void Start()
    {
        players = GameManager.GetInstance().GetPlayers();
        _navMeshAgent = GetComponent<NavMeshAgent>();
        _navMeshObstacle = GetComponent<NavMeshObstacle>();
        _animator = GetComponent<Animator>();
        _animateNavMeshAgent = GetComponent<AnimateNavMeshAgent>();
        _health = GetComponent<Health>();
        _navMeshAgent.updatePosition = false;
        collapseBones = GetComponent<CollapseBones>();
        audioSource = GetComponent<AudioSource>();
        var followHips = GetComponentInChildren<FollowHips>();
        if (followHips)
        {
            clickCollider = followHips.gameObject.GetComponent<Collider>();
        }
        if (weaponCollider != null)
        {
            weaponCollider.enabled = false;
        }
        eyes.SetActive(false);
    }


    private void OnEnable()
    {
        state = SkeletonState.SLEEPING;
    }

    private float GetPlayerRadius()
    {
        if (targetPlayer != null)
        {
            var playerNavAgent = targetPlayer.GetComponent<NavMeshAgent>();
            if (playerNavAgent != null)
            {
                return playerNavAgent.radius;
            }
        }
        return DEFAULT_PLAYER_RADIUS;
    }

    // Update is called once per frame
    void Update()
    {
        if (state == SkeletonState.WAKING_UP)
        {
            if (_animator.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
            {
                state = SkeletonState.STATIONARY;
            }
        }
        else if (state == SkeletonState.SLEEPING)
        {
            if (_animator.GetBool(Constants.IN_WAKEUP_RANGE))
            {
                state = SkeletonState.WAKING_UP;
                eyes.SetActive(true);
                wakingUpSound.Play(audioSource);
            }
        }
        else
        {
            float distanceToPlayer = float.MaxValue;
            if (targetPlayer == null)
            {
                foreach (var player in players)
                {
                    distanceToPlayer = Vector3.Distance(transform.position, player.transform.position);
                    if (distanceToPlayer < playerDetectionRange)
                    {
                        targetPlayer = player;
                        playerRadius = GetPlayerRadius();
                        playerHealth = GetPlayerHealth();
                        break;
                    }
                }
            }
            if (targetPlayer != null)
            {
                if (distanceToPlayer == float.MaxValue)
                {
                    distanceToPlayer = Vector3.Distance(transform.position, targetPlayer.transform.position);
                }
                if (distanceToPlayer < attackRange)
                {
                    Attack();
                }
                else if (distanceToPlayer < playerChaseRange)
                {
                    Chase();
                }
                else if (distanceToPlayer < playerDetectionRange && state != SkeletonState.SLEEPING)
                {
                    LookAt();
                }
                else if (state != SkeletonState.STATIONARY)
                {
                    state = SkeletonState.STATIONARY;
                    _animateNavMeshAgent.StopFollowingTargetRotationOnSpot();
                }
            }
        }

    }

    private Health GetPlayerHealth()
    {
        Health health = null;
        if (targetPlayer != null)
        {
            health = targetPlayer.GetComponent<Health>();
        }
        return health;
    }

    private void LookAt()
    {
        if (state != SkeletonState.LOOKING)
        {
            state = SkeletonState.LOOKING;
            _animateNavMeshAgent.FollowTargetRotationOnSpot(targetPlayer.transform);
        }
    }


    private void Chase()
    {
        if (_animator.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
        {
            if (state != SkeletonState.CHASING)
            {
                state = SkeletonState.CHASING;
                _navMeshAgent.destination = targetPlayer.transform.position;
                _animateNavMeshAgent.StopFollowingTargetRotationOnSpot();
                SetDestinationToPlayer();
            }
            else if ((targetPlayer.transform.position - _navMeshAgent.destination).sqrMagnitude > 0.01)
            {
                _navMeshAgent.destination = targetPlayer.transform.position;
            }
        }
    }

    private void SetDestinationToPlayer()
    {
                var movementVector = targetPlayer.transform.position - transform.position;
                var destination = transform.position +
                                  movementVector.normalized * (movementVector.magnitude - _navMeshAgent.radius - playerRadius -0.1f);
                _navMeshAgent.SetDestination(destination);
    }

    private void Attack()
    {
        if (state != SkeletonState.ATTACKING)
        {
            var normalizedMovementVector = (targetPlayer.transform.position - transform.position).normalized;
            _navMeshAgent.destination = transform.position + normalizedMovementVector * 0.1f;
            state = SkeletonState.ATTACKING;
            _animateNavMeshAgent.FollowTargetRotationOnSpot(targetPlayer.transform);
//            _navMeshAgent.enabled = false;
//            _navMeshObstacle.enabled = true;
            nextAttackTime = Time.time; // attack immediately upon changing state
        }
        if (Time.time > nextAttackTime)
        {
            _animator.SetTrigger("Attacking");
            SetNextAttackTime();
            wakingUpSound.Play(audioSource);
        }
    }

    private void SetNextAttackTime()
    {
        nextAttackTime = Time.time + attackCooldownTime + Random.value * maxRandomExtraCooldownTime;
    }

    public void EnableAttack()
    {
        weaponCollider.enabled = true;
    }

    public void DisableAttack()
    {
        weaponCollider.enabled = false;
    }

    public void TargetTriggerHit()
    {
        DisableAttack();
        ApplyDamage();
    }
    
    private void ApplyDamage()
    {
        if (playerHealth != null)
        {
            playerHealth.Damage(attackDamage);
        }
    }

    private float DistanceToTarget()
    {
        return Vector3.Distance(transform.position, targetPlayer.transform.position);
    }

    private float AngleToTarget()
    {
        return Vector3.SignedAngle(transform.forward, targetPlayer.transform.position - transform.position, Vector3.up);
    }

    private void OnDeath()
    {
        if (collapseBones != null)
        {
            _animator.enabled = false;
            _navMeshAgent.enabled = false;
            _animateNavMeshAgent.enabled = false;
            collapseBones.Collapse();
            if (clickCollider)
            {
                clickCollider.enabled = false;
            }
            eyes.SetActive(false);
            bonesCollapsingSound.Play(audioSource);
            
//            LeanPool.Despawn(this, 5f);
            Destroy(this.gameObject, 5f);
            
            this.enabled = false;
        }
    }
    
}


enum SkeletonState
{
    SLEEPING,
    WAKING_UP,
    STATIONARY,
    LOOKING,
    CHASING,
    ATTACKING
}
   