﻿using UnityEngine;

public class FollowHips : MonoBehaviour
{

	private Collider collider;
	public Transform hips;
	// Use this for initialization
	void Start ()
	{
		collider = GetComponent<Collider>();
	}
	
	// Update is called once per frame
	void Update () {
		if (collider && hips)
		{
			collider.transform.position = hips.transform.position;
			collider.transform.rotation = hips.transform.rotation;
		}
	}
}
