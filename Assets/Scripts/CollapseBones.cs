﻿using System.Collections.Generic;
using UnityEngine;

public class CollapseBones : MonoBehaviour
{

	private bool kinematicColliders = true;
	[Tooltip("Size of the side of the cube collider to be attached to the main bones."), Range(0.1f, 0.3f)]
	public float colliderSideSize = 0.1f;
	private Vector3 boxColliderSize;
	private static List<string> neededPartsList;
	private float nextKinematicTime;
	private const float DefaultKinematicTime = 3;

	static CollapseBones()
	{
		neededPartsList = new List<string>();
		neededPartsList.Add("mixamorig:Hips");
		neededPartsList.Add("mixamorig:Spine1");
		neededPartsList.Add("mixamorig:Head");
		neededPartsList.Add("mixamorig:RightForeArm");
		neededPartsList.Add("mixamorig:LeftForeArm");
		neededPartsList.Add("mixamorig:RightUpLeg");
		neededPartsList.Add("mixamorig:RightLeg");
		neededPartsList.Add("mixamorig:LeftUpLeg");
		neededPartsList.Add("mixamorig:LeftLeg");
	}

	// Use this for initialization
	void Start () {
		boxColliderSize = new Vector3(colliderSideSize, colliderSideSize, colliderSideSize);
		AddRigidbodyRecursive(transform);
		SetKinematic(true);
	}

	public void Collapse()
	{
		var animator = gameObject.GetComponent<Animator>();
		if (animator != null)
		{
			animator.enabled = false;
		}
		SetKinematic(false);
		kinematicColliders = false;
		nextKinematicTime = Time.time + DefaultKinematicTime;
	}
	
	
	// Update is called once per frame
	void Update () {
		if (!kinematicColliders && Time.time > nextKinematicTime)
		{
			SetKinematic(true);
			nextKinematicTime = Time.time + DefaultKinematicTime;
		}
	}

	private void SetKinematic(bool state)
	{
		foreach (var rigidbody in GetComponentsInChildren<Rigidbody>())
		{
			rigidbody.isKinematic = state;
			rigidbody.useGravity = !state;
			if (!rigidbody.gameObject.tag.Equals(Constants.ENEMY_WEAPON))
			{
				rigidbody.detectCollisions = !state;
			}
		}
		foreach (var collider in GetComponentsInChildren<Collider>())
		{
			if (!collider.gameObject.tag.Equals(Constants.ENEMY_WEAPON) &&
			    !collider.gameObject.tag.Equals(Constants.CLICK_COLLIDER))
			{
				collider.enabled = !state;
			}
		}
	}

	private bool IsNeeded(Transform partTransform)
	{
		bool isNeeded = false;
		string partName = partTransform.name;
		if (neededPartsList.Contains(partName))
		{
			isNeeded = true;
		}
		return isNeeded;
	}

	private void AddRigidbodyRecursive(Transform transformToChange)
	{
		for (int i = 0; i < transformToChange.childCount; i++)
		{
			AddRigidbodyRecursive(transformToChange.GetChild(i));
		}
		if (IsNeeded(transformToChange))
		{
			if (transformToChange.GetComponent<Rigidbody>() == null)
			{
				transformToChange.gameObject.AddComponent<Rigidbody>();
			}
			if (transformToChange.GetComponent<BoxCollider>() == null)
			{
				transformToChange.gameObject.AddComponent<BoxCollider>();
				var collider = transformToChange.GetComponent<BoxCollider>();
				collider.size = boxColliderSize;
			}
		}
	}
}
