﻿using UnityEngine;

[RequireComponent(typeof(Animator))]
public class WakeUpWhenInRange : MonoBehaviour
{
    private GameObject[] _players;
    private Animator _animator;
    [SerializeField] private float activationRange = 4f;
    [SerializeField] [Range(0f,5f)] private float wakeUpDelay = 1f;
    private float wakeUpTime;
    private bool aboutToWakeUp;
     

    // Use this for initialization
    void Start()
    {
        _players = GameObject.FindGameObjectsWithTag(Constants.PLAYER_TAG);
        _animator = GetComponent<Animator>();
    }

    private void OnEnable()
    {
        aboutToWakeUp = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!IsAwake())
        {
            if (aboutToWakeUp)
            {
                WakeUpIfDelayExpired();
            }
            else
            {
                CheckIfShouldWakeUp();
            }
        }
    }

    private void CheckIfShouldWakeUp()
    {
        foreach (var player in _players)
        {
            var distance = Vector3.Distance(player.transform.position, transform.position);
            if (distance < activationRange)
            {
                aboutToWakeUp = true;
                wakeUpTime = Time.time + wakeUpDelay;
                break;
            }
        }
    }

    private void WakeUpIfDelayExpired()
    {
        if (Time.time > wakeUpTime)
        {
            _animator.SetBool(Constants.IN_WAKEUP_RANGE, true);
        }
    }

    private bool IsAwake()
    {
        return _animator.GetBool(Constants.IN_WAKEUP_RANGE);
    }
}