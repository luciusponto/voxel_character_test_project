﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
	private static GameManager instance;
	private List<GameObject> players;

	public void LoadLevel(int levelIndex, GameObject loadingGraphic = null)
	{
		var asyncOperation = SceneManager.LoadSceneAsync(levelIndex);
	}

	public void StartGame()
	{
		Debug.Log("Loading level");
		SceneManager.LoadScene(2);
	}

	public void QuitGame()
	{
		Application.Quit();
	}
	
	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
			players = new List<GameObject>(1);
			DontDestroyOnLoad(gameObject);
		}
		else if (instance != this)
		{
			Destroy(gameObject);
		}
	}

	public void AddPlayer(GameObject player)
	{
		players.Add(player);
	}
	
	public void RemovePlayer(GameObject player)
	{
		players.Remove(player);
	}

	public List<GameObject> GetPlayers()
	{
		return players;
	}

	public static GameManager GetInstance()
	{
		return instance;
	}

	public void QuitToMainMenu()
	{
		SceneManager.LoadScene(1);
	}
}
