﻿using UnityEngine;

public abstract class CollisionEffect : ScriptableObject
{
    public abstract void Apply(GameObject target, Vector3 impactPoint);
}
