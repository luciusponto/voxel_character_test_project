﻿using Lean;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(AnimateNavMeshAgent))]
public class SpellCasting : MonoBehaviour
{
    private Animator animator;
    private AnimateNavMeshAgent navMeshAnimation;
    private Spell spell;
    private SpellCastingMechanism spellCastingMechanism;

    void Start()
    {
        animator = GetComponent<Animator>();
        navMeshAnimation = GetComponent<AnimateNavMeshAgent>();
        spellCastingMechanism = GetComponent<SpellCastingMechanism>();
    }

    public void TryCasting(GameObject spellPrefab, GameObject target, Collider targetCollider)
    {
        GameObject spellObject = LeanPool.Spawn(spellPrefab);
        spell = spellObject.GetComponent<Spell>();
        animator.SetBool(Constants.TRYING_TO_CAST, true);
        spell.caster = gameObject;
        spell.SetTarget(target, targetCollider);
        spell.Bind(gameObject);
        var targetNavMesh = target.GetComponentInParent<NavMeshAgent>();
        var followTarget = target.transform;
        if (targetNavMesh != null)
        {
            followTarget = targetNavMesh.transform;
        }
        navMeshAnimation.FollowTargetRotationOnSpot(followTarget.transform);
        spellCastingMechanism.TryToCast(spell, animator);
    }

    public void Cast()
    {
        spell.Cast();
    }

    public void NotCasting()
    {
        animator.SetBool(Constants.CASTING, false);
        navMeshAnimation.StopFollowingTargetRotationOnSpot();
    }
}
