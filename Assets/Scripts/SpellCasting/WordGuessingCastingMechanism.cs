﻿using UnityEngine;

public class WordGuessingCastingMechanism : SpellCastingMechanism, IWordGuessingListener
{

    public override void TryToCast(Spell spell, Animator animator)
    {
        base.TryToCast(spell, animator);
        WordGuessingManager.Instance.GuessWord();
    }

    public void OnStartGuessing(WordGuessingStatus status)
    {
        // do nothing
    }

    public void OnFinishedGuessing(WordGuessingStatus status)
    {
        if (status.IsSuccessful())
        {
            CastSpell();
        }
        else
        {
            CancelSpell();
        }
    }

    public void OnUpdate(WordGuessingStatus status)
    {
        float mistakes;
        if (status.Mistakes == status.MaxMistakes - 1)
        {
            mistakes = 1f;
        }
        else
        {
            mistakes = (float) status.Mistakes / status.MaxMistakes;
        }
        
        spell.UpdateVisualEffects((float)status.TotalCorrectLetters/status.Word.UniqueLetterCount(), mistakes);
    }

    private void Start()
    {
        WordGuessingManager.Instance.RegisterListener(this);
    }
    
    private void OnDestroy()
    {
        WordGuessingManager.Instance.RemoveListener(this);
    }
    
}

