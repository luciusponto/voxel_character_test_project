﻿using System.Collections;
using Lean;
using UnityEngine;

public class Spell : MonoBehaviour
{
    public GameObject caster;
    public GameObject targetObject;
    private Vector3 targetCenterOfMass;
    public float speed = 10;
    public int angularAcceleration;
    private bool seeking;
    private float progressToSuccess = 0;
    private float progressToFailure = 0;
    private float offsetX = 0;
    private float offsetY = 0.054f;
    private float offsetZ = 0.158f;
    private static Vector3 offset;
    public ParticleSystem castingEffectPrefab;
    public ParticleSystem castingFailureEffectPrefab;
    public ParticleSystem collisionVisualEffectPrefab;
    private ParticleSystem castingEffectInstance;
    private ParticleSystem castingFailureEffectInstance;
    public CollisionEffect effect;
    private Collider targetCollider;
    private float minCastingEffectSize;
    [SerializeField] [Range(1f, 3f)] private float maxCastingEffectSizeMultiplier;
    [SerializeField] private AudioClip spellCastingStart;
    [SerializeField] private AudioClip spellCastingLoop;
    [SerializeField] private AudioClip spellCastingEnd;
    [SerializeField] private AudioClip spellFired;
    [SerializeField] private AudioClip spellHit;
    private AudioSource audioSource;
    

    [SerializeField]
    [Tooltip("mistake values larger than this will make the casting failure effect play without breaks")]
    [Range(0f, 1f)] private float constantFailureEffectThreshold;

    [SerializeField]
    [Tooltip("mistake values larger than this will make the casting failure effect play without breaks")]
    [Range(0f, 1f)] private float minimumMistakesForFailureEffect;


    // Use this for initialization
    void Start()
    {
        offset = new Vector3(offsetX, offsetY, offsetZ);
        GetCastingEffect();
        GetFailureEffect();
        audioSource = GetComponent<AudioSource>();
    }

    private ParticleSystem GetFailureEffect()
    {
        if (castingFailureEffectInstance == null)
        {
            castingFailureEffectInstance =
                LeanPool.Spawn(castingFailureEffectPrefab, Vector3.zero, Quaternion.identity, transform);
            castingFailureEffectInstance.GetComponent<SmokePuffs>().SetActiveFractionOfPeriod(0f);
        }
        return castingFailureEffectInstance;
    }

    private void OnEnable()
    {
        if (audioSource == null)
        {
            audioSource = GetComponent<AudioSource>();
        }
        AudioManager.PlaySound(audioSource, spellCastingStart);
        StartCoroutine(PlaySoundLoop(spellCastingStart.length));
        if (castingEffectInstance != null)
        {
            castingEffectInstance.Clear();
            castingEffectInstance.Play();
            castingEffectInstance.gameObject.SetActive(true);            
        }
        if (castingFailureEffectInstance != null)
        {
            castingFailureEffectInstance.Clear();
            castingFailureEffectInstance.Play();
            castingFailureEffectInstance.gameObject.SetActive(true);      
        }
    }

    private IEnumerator PlaySoundLoop(float delay)
    {
        float timeToPlay = Time.time + delay;
        while (Time.time < timeToPlay)
        {
            yield return null;
        }
        AudioManager.PlaySound(audioSource, 1.0f, spellCastingLoop, true);
    }

    private ParticleSystem GetCastingEffect()
    {
        if (castingEffectInstance == null)
        {
            castingEffectInstance = LeanPool.Spawn(castingEffectPrefab, Vector3.zero, Quaternion.identity, transform);
        }
        minCastingEffectSize = castingEffectInstance.main.startSize.constant;
        return castingEffectInstance;
    }

    // Update is called once per frame
    void Update()
    {
        if (seeking && targetObject != null)
        {
            MoveToTarget();
        }
    }

    /// <summary>
    /// Updates visual effects according to spell casting progress
    /// </summary>
    /// <param name="progress">0 = casting has just started, 1 = casting has finished</param>
    /// <param name="mistakes">0 = no mistakes have been made in casting so far. 1 = Too many mistakes have been made and the casting will fail.</param>
    public void UpdateVisualEffects(float progress, float mistakes)
    {
        SetupCastingEffect(progress);
        SetupFailureEffect(mistakes);
    }

    private void SetupCastingEffect(float progress)
    {
        // do nothing
    }

    private void SetupFailureEffect(float mistakes)
    {       
        if (mistakes > 0.99f)
        {
            GetFailureEffect().GetComponent<SmokePuffs>().SetActiveFractionOfPeriod(1f);
        }
        else
        {
            GetFailureEffect().GetComponent<SmokePuffs>().SetActiveFractionOfPeriod(mistakes/2f);
        }
    }

    private void MoveToTarget()
    {
        Vector3 impactPoint;
        if (targetCollider != null)
        {
            impactPoint = targetCollider.ClosestPoint(transform.position);
        }
        else
        {
            impactPoint = transform.position;
        }
        transform.LookAt(impactPoint);
        var remainingDistance = Vector3.Distance(transform.position, impactPoint);
        var updateTranslationDistance = Mathf.Clamp(speed * Time.deltaTime, 0f, remainingDistance);
        transform.Translate(Vector3.forward * updateTranslationDistance);
        if (remainingDistance < 0.05f)
        {
            seeking = false;
            AudioManager.PlaySound(audioSource, spellHit);
            ApplyEffect(impactPoint);
            PlayCollisionVisualEffect(impactPoint);
            ReturnToPool(spellHit.length);
        }
    }

    private void PlayCollisionVisualEffect(Vector3 impactPoint)
    {
        var movementDirection = caster.transform.position - targetObject.transform.position;
        var collisionVisualEffectInstance = LeanPool.Spawn(collisionVisualEffectPrefab, impactPoint, Quaternion.LookRotation(movementDirection), null);
        var collisionMain = collisionVisualEffectInstance.main;
        collisionMain.startColor = castingEffectInstance.colorOverLifetime.color.Evaluate(1);
        collisionVisualEffectInstance.Play();
        LeanPool.Despawn(collisionVisualEffectInstance, collisionVisualEffectInstance.main.duration);
    }

    private ParticleSystem GetCollisionVisualEffect()
    {
        throw new System.NotImplementedException();
    }

    private void ApplyEffect(Vector3 impactPoint)
    {
        effect.Apply(targetObject, impactPoint);
    }

    public void Bind(GameObject caster)
    {
        EquipmentAttachmentPoints attachmentPoints = caster.GetComponent<EquipmentAttachmentPoints>();
        if (attachmentPoints == null)
        {
            ReturnToPool(0f);
        }
        else
        {
            transform.SetParent(attachmentPoints.GetSpellAttachmentPoint());
            transform.localPosition = Vector3.up * 0.3f;
            //            transform.position = offset;
        }
    }

    public void SetTarget(GameObject target, Collider targetCollider)
    {
        this.targetObject = target;
        this.targetCollider = targetCollider;
    }

    private void SeekTarget()
    {
        transform.SetParent(null);
        seeking = true;
        StopCoroutine(PlaySoundLoop(spellCastingStart.length));
        AudioManager.PlaySound(audioSource, spellFired);
    }

    public void Cast()
    {
        StopCastingFailureEffect();
        SeekTarget();
    }

    private void StopCastingFailureEffect()
    {
        castingFailureEffectInstance.GetComponent<SmokePuffs>().SetActiveFractionOfPeriod(0f);
    }

    public void Cancel()
    {
        StopCoroutine(PlaySoundLoop(spellCastingStart.length));
        AudioManager.PlaySound(audioSource, spellCastingEnd);
        ReturnToPool(spellCastingEnd.length);
    }

    void ReturnToPool(float delay)
    {
        castingEffectInstance.gameObject.SetActive(false);
        castingFailureEffectInstance.gameObject.SetActive(false);
        LeanPool.Despawn(gameObject, delay);
    }
}