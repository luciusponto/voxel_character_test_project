﻿using UnityEngine;

[CreateAssetMenu(fileName = "DamageSpellEffect", menuName = "Effect/Spell/Damage")]
public class DamageSpellEffect : CollisionEffect
{
    public int damage = 1;
    public float explosionForce = 100;
    public float explosionRadius = 0.5f;

    public override void Apply(GameObject target, Vector3 impactPoint)
    {
        var health = target.transform.root.GetComponent<Health>();
        if (health != null)
        {
            health.Damage(damage);
        }
        if (impactPoint != null)
        {
            foreach (var rigidbody in target.GetComponentsInChildren<Rigidbody>())
            {
                rigidbody.AddExplosionForce(explosionForce, impactPoint, explosionRadius);
            }
        }

    }
}
