﻿using UnityEngine;

public abstract class SpellCastingMechanism : MonoBehaviour
{
	protected Spell spell;
	protected Animator animator;
	protected bool tryingToCast;
	
	private void PlayCastingAnimation()
	{
		animator.SetBool(Constants.CASTING, true);
	}

	private void StopTryingToCastAnimation()
	{
		animator.SetBool(Constants.TRYING_TO_CAST, false);
	}
	
	public virtual void TryToCast(Spell spell, Animator animator)
	{
		this.spell = spell;
		this.animator = animator;
		tryingToCast = true;
	}

	protected void CancelSpell()
	{
		spell.Cancel();
		StopTryingToCastAnimation();
		tryingToCast = false;
	}

	protected void CastSpell()
	{
		PlayCastingAnimation();
		tryingToCast = false;
	}
}
