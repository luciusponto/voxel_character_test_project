﻿using Lean.Touch;
using UnityEngine;

public class ClickCastingMechanism : SpellCastingMechanism {
    
    private void Update()
    {
        if (tryingToCast)
        {
            if (Input.GetMouseButtonDown(0) && ClickHitCollider())
            {
                CastSpell();
            }
            if (Input.GetMouseButtonDown(1))
            {
                CancelSpell();
            }
        }

    }

    private bool ClickHitCollider()
    {
        if (LeanTouch.PointOverGui(Input.mousePosition))
        {
            return false;
        }
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, 100))
        {
            return true;
        }
        return false;
    }
}
