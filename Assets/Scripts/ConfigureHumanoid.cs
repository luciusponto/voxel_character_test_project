﻿using System.Collections.Generic;
using UnityEngine;

public class ConfigureHumanoid : MonoBehaviour
{
	public float VoxelSize = 0.1f;
	private string BODY = "Body";
	private string BODY_PARTS_FILE_NAME = "bodyParts";
	private List<BodyPart> bodyParts;

	void Initialize()
	{
		bodyParts = new List<BodyPart>();
		TextAsset textAsset = Resources.Load<TextAsset>(BODY_PARTS_FILE_NAME);
		var lines = textAsset.text.Split('\n');
		foreach (var line in lines)
		{
			var bodyPart = JsonUtility.FromJson<BodyPart>(line);
			if (bodyPart != null)
			{
				bodyParts.Add(bodyPart);
			}
		}
	}

	[ContextMenu ("Configure humanoid character")]
	void Configure()
	{
		Initialize();
		var copy = Instantiate(gameObject);
		copy.name = copy.name + "_configured";
		var body = gameObject.transform.Find(BODY);
		if (body == null)
		{
			var newGameObject = new GameObject();
			newGameObject.name = BODY;
			newGameObject.transform.SetParent(gameObject.transform);
			body = newGameObject.transform;
		}
		foreach (var bodyPart in bodyParts)
		{
			Debug.Log(bodyPart.name);
		}
	}

}

internal class BodyPart
{
	public string name = "";
	public string parent = "";
	public float xoffset = 0;
	public float yoffset = 0;
	public float zoffset = 0;
	public float scale = 0;
}
