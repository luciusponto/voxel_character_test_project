﻿using System.Collections.Generic;
using System.Text;

public class WordGuessingStatus
{
    public WordForGuessing Word { get; private set; }
    public int MaxMistakes { get; private set; }
    public int TotalCorrectLetters { get; private set; }
    public int CorrectUniqueLetters { get; private set; }
    public int Mistakes { get; private set; }
    public string GuessedSoFar { get; private set; }
    private List<char> correctUniqueLetters;
    private readonly StringBuilder guessedSoFarBuilder = new StringBuilder();
    public HashSet<char> MissingLetters { get; private set; }
    public HashSet<char> GuessedLetters { get; private set; }
    public List<char> WrongLetters { get; private set; }
    public char LatestPlayedLetter { get; private set; }
    public bool LatestLetterSuccessful { get; private set; }


    public WordGuessingStatus()
    {
        MissingLetters = new HashSet<char>();
        GuessedLetters = new HashSet<char>();
        WrongLetters = new List<char>(WordGuessingManager.MaxWordLength);
    }

    public void Initialize(WordForGuessing word, int maxMistakes)
    {
        Word = word;
        MaxMistakes = maxMistakes;
        Mistakes = 0;
        LatestPlayedLetter = (char)0;
        MissingLetters.Clear();
        GuessedLetters.Clear();
        WrongLetters.Clear();
        PopulateLetterSets();
        UpdateGuessedSoFar();
    }

    private void PopulateLetterSets()
    {
        var caseInsensitiveWord = WordUtil.RemoveCase(Word.Word);
        foreach (var letter in caseInsensitiveWord)
        {
            MissingLetters.Add(letter);
        }
    }

    private void UpdateGuessedSoFar()
    {
        guessedSoFarBuilder.Clear();
        foreach (var letter in Word.Word)
        {
            if (GuessedLetters.Contains(WordUtil.RemoveCase(letter)))
            {
                guessedSoFarBuilder.Append(letter);
            }
            else
            {
                guessedSoFarBuilder.Append(WordGuessingManager.IncognitoCharacter);
            }
        }
        GuessedSoFar = guessedSoFarBuilder.ToString();
    }
    
    public void Update(char letter)
    {
        LatestPlayedLetter = letter;
        GuessedLetters.Add(letter);
        if (LetterCorrectlyGuessed(letter))
        {
            MissingLetters.Remove(letter);
            LatestLetterSuccessful = true;
            TotalCorrectLetters++;
        }
        else
        {
            WrongLetters.Add(letter);
            Mistakes++;
            LatestLetterSuccessful = false;
        }
        UpdateGuessedSoFar();

    }

    private bool LetterCorrectlyGuessed(char letter)
    {
        return MissingLetters.Contains(letter);
    }

    public bool IsSuccessful()
    {
        return MissingLetters.Count == 0;
    }

    public bool IsFailed()
    {
        return Mistakes >= MaxMistakes;
    }
}