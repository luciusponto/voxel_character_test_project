﻿using UnityEngine;

[CreateAssetMenu(fileName = "WordDifficulty", menuName = "Word Guessing/Difficulty")]
public class WordDifficulty: ScriptableObject
{
    [Range(0,1f)] public float difficulty;
}