using UnityEngine;

/// <summary>
/// 
/// Empty Scriptable Object, meant to be used as an editor-extensible enum.
/// </summary>
[CreateAssetMenu(fileName = "WordCategory", menuName = "Word Guessing/Word Category")]
public class WordCategory: ScriptableObject
{
    private void OnValidate()
    {
        CheckCorrespondingFileExists();
    }

    private void CheckCorrespondingFileExists()
    {
        Debug.Log("Checking that word list exists for category " + name);
        var path = "wordLists/en-GB/" + name;
        var wordListData = Resources.Load(path) as TextAsset;
        Debug.Assert(wordListData != null, "Could not find word category file: " + path);
    }
}