using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[CreateAssetMenu(fileName = "RandomWordProvider", menuName = "Word Guessing/Word Provider/Random")]
public class RandomWordProvider : WordProvider
{
    [NonSerialized] private WordCategory currentCategory;
    [NonSerialized] private List<string> wordList;

    private void Awake()
    {
        currentCategory = null;
    }

    public override WordForGuessing GetWord(WordParameters parameters)
    {
        if (CategoryChanged(parameters))
        {
            LoadWordListFromFile(parameters);
        }
        var wordString = wordList[Random.Range(0, wordList.Count)].Trim();
        return new WordForGuessing(parameters.Category, parameters.Difficulty, wordString);
    }

    private bool CategoryChanged(WordParameters parameters)
    {
        if (currentCategory == null || !currentCategory.Equals(parameters.Category))
        {
            currentCategory = parameters.Category;
            return true;
        }
        return false;
    }

    private void LoadWordListFromFile(WordParameters parameters)
    {
        var filePath = WordsRoot + "en-GB/" + parameters.Category.name;
        Debug.Log("Trying to load word list from " + filePath);
        TextAsset txt = (TextAsset)Resources.Load(filePath, typeof(TextAsset));
        string content = txt.text;
        var wordArray = content.Split(LineSeparator);
        wordList = new List<string>(wordArray.Length);
        foreach (var word in wordArray)
        {
            if (parameters.Validator.IsValid(word))
            {
                wordList.Add(word);
            }
        }
    }
}