﻿using System;

public class WordUtil
{
    public static char RemoveCase(char letter)
    {
        return Char.ToLower(letter);
    }

    public static string RemoveCase(string caseSensitiveString)
    {
        return caseSensitiveString.ToLower();
    }
}