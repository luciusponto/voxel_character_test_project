﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class WordGuessingManager : MonoBehaviour {
	private bool guessing;
	private WordForGuessing currentWord;
	private WordGuessingStatus status;
	public const int MaxWordLength = 20;
	public int MaxMistakes = 3;
	private List<IWordGuessingListener> listeners;
	private Queue<char> inputLetters;
	private OnScreenKeyboard onScreenKeyboard;
	public static WordGuessingManager Instance;
	public GameObject OnScreenKeyboardPrefab;
	public const char IncognitoCharacter = '_';
	private int currentIndex;
	AudioSource audioSource;
	[SerializeField] private AudioEvent successfulLetterAudio;
	[SerializeField] private AudioEvent failedLetterAudio;
	[SerializeField] private WordParameters wordParameters;
	[SerializeField] private WordProvider wordProvider;

	// Update is called once per frame
	void Update ()
	{
		if (guessing)
		{
			ReadInputCharacters();
			ProcessNextLetter();
		}
	}

	private void ReadInputCharacters()
	{
		ReadInputCharacters(Input.inputString); 						// physical keyboard input
		ReadInputCharacters(GetOnScreenKeyboard().GetInputString());	// on screen keyboard input
	}

	private void ReadInputCharacters(string charactersString)
	{
		foreach (char inputCharacter in charactersString)
		{
			var caseInsensitiveCharacter = WordUtil.RemoveCase(inputCharacter);
			if (Char.IsLetter(caseInsensitiveCharacter) && !status.GuessedLetters.Contains(caseInsensitiveCharacter) && !status.WrongLetters.Contains(caseInsensitiveCharacter))
			{
				inputLetters.Enqueue(caseInsensitiveCharacter);
			}
		}
	}

	private OnScreenKeyboard GetOnScreenKeyboard()
	{
		if (OnScreenKeyboard.Instance == null)
		{
			Instantiate(OnScreenKeyboardPrefab);
		}
		onScreenKeyboard = OnScreenKeyboard.Instance;
		if (onScreenKeyboard == null)
		{
			
			Debug.LogError("Could not find on screen keyboard instance");
		}
		return onScreenKeyboard;
	}

	private void ProcessNextLetter()
	{
		if (inputLetters.Count > 0)
		{
			var letter = inputLetters.Dequeue();
			CheckLetter(letter);
		}
	}

	private void CheckLetter(char letter)
	{
		status.Update(letter);
		PlayLetterSoundFx(status.LatestLetterSuccessful);
		CheckResult();
		foreach (var listener in listeners)
		{
			listener.OnUpdate(status);
		}
	}

	private void PlayLetterSoundFx(bool successful)
	{
		if (successful)
		{
			successfulLetterAudio.Play(audioSource);
		}
		else
		{
			failedLetterAudio.Play(audioSource);
		}
	}

	private void CheckResult()
	{
		if (status.IsSuccessful())
		{
			NotifySuccess();
			guessing = false;
		} else if (status.IsFailed())
		{
			NotifyFailure();
			guessing = false;
		}
	}

	private void NotifyFailure()
	{
		foreach (var listener in listeners)
		{
			listener.OnFinishedGuessing(status);
		}
	}

	private void NotifySuccess()
	{
		foreach (var listener in listeners)
		{
			listener.OnFinishedGuessing(status);
		}
	}

	public void CancelCurrentWord()
	{
		guessing = false;
		NotifyFailure();
	}

	private void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
			DontDestroyOnLoad(gameObject);
			inputLetters = new Queue<char>(MaxWordLength);
			listeners = new List<IWordGuessingListener>();
		}
		else
		{
			Destroy(gameObject);
		}
	}


	private void Start()
	{
		status = new WordGuessingStatus();
		audioSource = gameObject.GetComponent<AudioSource>();
	}

	public void RegisterListener(IWordGuessingListener listener)
	{
		listeners.Add(listener);
	}

	public void RemoveListener(IWordGuessingListener listener)
	{
		listeners.Remove(listener);
	}

	public void GuessWord()
	{
		currentWord = GetNextWord();
		status.Initialize(currentWord, MaxMistakes);
		foreach (var subscriber in listeners)
		{
			subscriber.OnStartGuessing(status);
			subscriber.OnUpdate(status);
		}
		ConsumeUnusedLetters();
		guessing = true;
	}

	private void ConsumeUnusedLetters()
	{
		var unused = Input.inputString; // consume and do nothing with it
		GetOnScreenKeyboard().GetInputString(); // do nothing with it
	}

	private WordForGuessing GetNextWord()
	{
		return wordProvider.GetWord(wordParameters);
	}

}