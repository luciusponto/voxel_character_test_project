﻿using UnityEngine;

public abstract class WordProvider: ScriptableObject
{
    public const string WordsRoot = "wordLists/";
    public const char LineSeparator = '\n';
    public abstract WordForGuessing GetWord(WordParameters parameters);
}