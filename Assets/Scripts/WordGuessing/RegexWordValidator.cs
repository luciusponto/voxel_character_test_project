using System.Text.RegularExpressions;
using UnityEngine;

[CreateAssetMenu(fileName = "RegexWordValidator", menuName = "Word Guessing/Validator/Regex")]
public class RegexWordValidator : WordValidator
{
    private Regex validatorRegex; 
    
    [SerializeField] private string regularExpression;
    [SerializeField] private string[] validWordsForTesting;
    [SerializeField] private string[] invalidWordsForTesting;

    public override bool IsValid(string word)
    {
        if (validatorRegex == null)
        {
            validatorRegex = GetRegex();
        }
        return validatorRegex.IsMatch(word);        
    }

    private Regex GetRegex()
    {
        return new Regex(regularExpression);
    }

    private void OnValidate()
    {
        TestExpression();    
    }


    public void TestExpression()
    {
        Debug.Log("Modified validator data. Testing regular expression \"" + regularExpression + "\" against " + validWordsForTesting.Length + " valid and " + invalidWordsForTesting.Length + " invalid words.");
        foreach (var word in validWordsForTesting)
        {
            Debug.Assert(IsValid(word), "The regular expression \"" + regularExpression + "\" does not match the valid test word " + word);
        }
        foreach (var word in invalidWordsForTesting)
        {
            Debug.Assert(!IsValid(word), "The regular expression \"" + regularExpression + "\" matches the invalid test word " + word);
        }
    }
}
