﻿public interface IWordGuessingListener
{
    void OnStartGuessing(WordGuessingStatus status);
    void OnFinishedGuessing(WordGuessingStatus status);
    void OnUpdate(WordGuessingStatus status);
}