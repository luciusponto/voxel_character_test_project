﻿using System;
using UnityEngine;

public abstract class WordValidator: ScriptableObject
{
    public abstract bool IsValid(string word);
}

internal class IncorrectRegexExpression : Exception
{
    public IncorrectRegexExpression(string message) : base(message)
    {
    }

}
