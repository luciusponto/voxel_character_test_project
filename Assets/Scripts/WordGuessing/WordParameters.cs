﻿using System;

[Serializable]
public class WordParameters
{
    public WordCategory Category;
    public WordDifficulty Difficulty;
    public PlayerSkills PlayerSkills;
    public WordValidator Validator;
    
}