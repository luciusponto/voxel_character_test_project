﻿using System;
using System.Collections.Generic;

[Serializable]
public class WordForGuessing
{
    public WordCategory Category { get; private set; }
    public WordDifficulty Difficulty { get; private set; }
    public string Word { get; private set; }
    public int TotalLetters { get; private set; }
    public HashSet<char> UniqueLetters { get; private set; }

    public int UniqueLetterCount()
    {
        return UniqueLetters.Count;
    } 

    public WordForGuessing(WordCategory category, WordDifficulty difficulty, string word)
    {
        Category = category;
        Difficulty = difficulty;
        Word = word;
        TotalLetters = word.Length;
        PopulateUniqueLetters();
    }

    private void PopulateUniqueLetters()
    {
        UniqueLetters = new HashSet<char>();
        foreach (var letter in Word)
        {
            UniqueLetters.Add(letter);
        }
    }

    public override string ToString()
    {
        return Word;
    }

    public override int GetHashCode()
    {
        return Category.GetHashCode() * Word.GetHashCode();
    }

    public override bool Equals(object obj)
    {
        var equals = false;
        if (obj.GetType().Equals(this.GetType()))
        {
            var other = (WordForGuessing) obj;
            if (other.Word.Equals(Word) && other.Category.Equals(Category))
            {
                equals = true;
            }
        }
        return equals;
    }
}
