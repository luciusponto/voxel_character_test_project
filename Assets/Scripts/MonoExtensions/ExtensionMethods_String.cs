﻿using System.Text;

public static class ExtensionMethods_String
{
    public static void Clear ( this StringBuilder instance )
    {// #Hack this shall reset the stringbuilder without releasing the memory, .net 4.0 adds a clear method which is not available here, maybe when unity's mono is updated, at least it can be made compatible by removing this method source: http://www.mindfiresolutions.com/A-better-way-to-Clear-a-StringBuilder-951.php
        instance.Length = 0;
    }
}
