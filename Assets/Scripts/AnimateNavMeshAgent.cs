﻿using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(NavMeshAgent))]
public class AnimateNavMeshAgent : MonoBehaviour {

    private NavMeshAgent agent;
    private Animator animator;
    private Transform followAngleTarget;
    private Vector3 followAngleTargetPosition;
    private float followAngleTargetUpdateTime;
    private float currentTargetFollowAngle;
    private float currentSpeed;
    private float followAngeUpdatePeriod = 0.5f;
    private double maxDistanceToUpdateAnimation = 0.1f;
    [SerializeField, Range(0,500)]
    [Tooltip("Max character angular speed when turning on the spot to face a target")]
    private float maxFollowAngularSpeed = 500;
    private float remainingFollowAngleAjustment = 0f;
    private bool followingTargetRotationOnSpot;
    [SerializeField, Range(0,1)]
    [Tooltip("How smoothly a character's angular speed will change when turning on the spot to face a target")]
    private float angleSmoothing = 0.125f;
    private float angularSpeed = 0f;
    [SerializeField, Range(0,2)]
    [Tooltip("How long it takes for a character to turn on the spot towards a target. Some of this time will be used only smoothing the animation but not changing much the character's actual rotation.")]
    private float angleSmoothingTime = 1f;
    private float currentAnimatorSpeed;
    [SerializeField, Range(0.1f,0.9f)]
    [Tooltip("How fast the animation will transition from walking/running back to idle.")]
    private float speedSmoothingFactor = 0.8f;
    [SerializeField, Range(0.005f,0.1f)]
    [Tooltip("Walking animation speeds below this will instantlty snap back to zero.")]
    private float minSpeedToSmoothDown = 0.05f;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        agent.updatePosition = false;
        animator = GetComponent<Animator>();
        maxFollowAngularSpeed = agent.angularSpeed;
        maxDistanceToUpdateAnimation = Mathf.Max(0.1f, (agent.stoppingDistance - 0.9f));
        followAngleTargetUpdateTime = Time.time;
    }

    void Update()
    {
        if (agent.remainingDistance > agent.radius)
        {
            agent.updateRotation = true;
        }
        else
        {
            agent.updateRotation = false;
            agent.destination = transform.position; // stops agent from jittering if another agent is very close to the destination.
        }
    }

    void FixedUpdate()
    {
        var turn = 0f;
        if (followingTargetRotationOnSpot)
        {
            // ignore high frequency oscillations in target position
            if (Time.time - followAngleTargetUpdateTime > followAngeUpdatePeriod)
            {
                currentTargetFollowAngle =
                    Vector3.SignedAngle(transform.forward, followAngleTarget.position - transform.position, Vector3.up);
                followAngleTargetUpdateTime = Time.time + followAngeUpdatePeriod;
            }
            var smoothedAngle = Mathf.SmoothDampAngle(0f, currentTargetFollowAngle, ref angularSpeed, angleSmoothingTime,
                maxFollowAngularSpeed);
            transform.Rotate(Vector3.up, smoothedAngle);
            if (Mathf.Abs(currentTargetFollowAngle) > 0.5f)
            {
                turn = smoothedAngle;
            }
            currentTargetFollowAngle -= smoothedAngle;
        }
        var currentAgentSpeed = agent.velocity.magnitude;
        // The nav agent will reduce speed really quickly.
        // Reduce animator speed more gradually so that animation doesn't snap from running to idle.
        // But let it snap to zero once it is sufficiently close to zero to no longer make a difference.
        if (currentAnimatorSpeed > minSpeedToSmoothDown && currentAgentSpeed < currentAnimatorSpeed * speedSmoothingFactor)
        {
            currentAgentSpeed = currentAnimatorSpeed * speedSmoothingFactor;
        }
        currentAnimatorSpeed = currentAgentSpeed;
        animator.SetFloat(Constants.TURN, turn);
        animator.SetFloat(Constants.SPEED, currentAnimatorSpeed);
    }

    public void FollowTargetRotationOnSpot(Transform target)
    {
        followAngleTarget = target;
        followingTargetRotationOnSpot = true;
    }

    public void StopFollowingTargetRotationOnSpot()
    {
        followingTargetRotationOnSpot = false;
    }

    private void OnAnimatorMove()
    {
        if (agent.velocity.sqrMagnitude > 0.01f)
        {
            transform.position = agent.nextPosition;
        }
    }

}
