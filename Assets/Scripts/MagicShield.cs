﻿using System.Collections.Generic;
using Lean;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(BoxCollider))]
public class MagicShield : MonoBehaviour
{
	[SerializeField, Tooltip("Shield color when it is full.")]
	private Color fullColor;
	[SerializeField, Tooltip("Shield color when it is half-full.")]
	private Color halfFullColor;
	[SerializeField, Tooltip("Shield color when it is empty.")]
	private Color emptyColor;
	[SerializeField, Range(0, 100), Tooltip("When health is below this percentage, shield will flicker")]
	private int minHealthPercentForFlicker = 20;
	[SerializeField, Range(0.5f, 2f), Tooltip("When health is low the shield will flicker. This is the period of the flicker oscillation.")]
	private float lowHealthFlickerPeriod = 1f; 
	[SerializeField, Range(0, 1f), Tooltip("Fraction of the flicker period when the particle shows")]
	private float lowHealthFlickerOnFraction = 0.5f;
	[SerializeField, Range(3,100), Tooltip("Number of color bands representing health. More bands make the shield change color more gradually and also increase memory usage. Updated only when game is restarted.")]
	private int totalColorBands = 3;
	[SerializeField, Range(5,30)]
	private int maxSparks = 30;
	public ParticleSystem mainParticlesPrefab;
	private ParticleSystem mainParticles;
	public ParticleSystem impactParticlesPrefab;
	private int healthPercentage;
	private float flickerToggleTime;
	private bool shieldVisible;
	private float visibleAlpha = 214f;
	private GradientAlphaKey[] visibleAlphaKeys;
	private GradientAlphaKey[] sineAlphaKeys;
	private GradientAlphaKey[] invisibleAlphaKeys;
	private Dictionary<int, GradientColorKey[]> colorBands;
	[SerializeField] private AudioEvent hitSound;
	private AudioSource audioSource;

	// Use this for initialization
	void Start ()
	{
		mainParticles = Instantiate(mainParticlesPrefab, gameObject.transform);
		if (mainParticles != null)
		{
			var color = mainParticles.colorOverLifetime.color;
			sineAlphaKeys = color.gradient.alphaKeys;
			invisibleAlphaKeys = new GradientAlphaKey[] {new GradientAlphaKey(0, 0)};
			visibleAlphaKeys = new GradientAlphaKey[] {new GradientAlphaKey(visibleAlpha, 0)};
		}
		UpdateShieldColor(mainParticles.colorOverLifetime.color.gradient.colorKeys, sineAlphaKeys);
		flickerToggleTime = Time.time + lowHealthFlickerPeriod * lowHealthFlickerOnFraction;
		healthPercentage = 100;
		InitializeColorBands();
		audioSource = GetComponent<AudioSource>();
	}

	private void InitializeColorBands()
	{
		colorBands = new Dictionary<int, GradientColorKey[]>(totalColorBands);
		var middleBandIndex = (totalColorBands - 1)/2;
		for (int i = 0; i < totalColorBands; i++)
		{
			Color shieldColor;
			if (i > middleBandIndex)
			{
				var t = (i - middleBandIndex) / (float)(totalColorBands - 1 - middleBandIndex);
				shieldColor = Color.Lerp(halfFullColor, fullColor, t); // 100% health maps to t=0 (full color); 50% maps to t=1 (half-full color)
			}
			else
			{
				var t = i / (float)middleBandIndex;
				shieldColor = Color.Lerp(emptyColor, halfFullColor, t); // 50% health maps to t=0 (half-full color); 0% health maps to t=1 (empty color)
			}
			colorBands.Add(i, new GradientColorKey[] {new GradientColorKey(shieldColor, 0f)});
		}
	}

	private static int DivideRoundedUp(int divident, int divisor)
	{
		int roundedUpRemainder = divident % divisor >= 1 ? 1 : 0;
		return divident / divisor + roundedUpRemainder;
	}

	void SetVisible(bool isVisible)
	{
		shieldVisible = isVisible;
		if (mainParticles != null)
		{
			var alphaKeys = isVisible ? visibleAlphaKeys : invisibleAlphaKeys;
			UpdateShieldColor(mainParticles.colorOverLifetime.color.gradient.colorKeys, alphaKeys);
		}
	}
	
	private void FixedUpdate()
	{
		if (healthPercentage > 0 && healthPercentage <= minHealthPercentForFlicker)
		{
			if (Time.time >= flickerToggleTime)
			{
				if (shieldVisible)
				{
					SetVisible(false);
					flickerToggleTime = Time.time +  lowHealthFlickerPeriod * (1 - lowHealthFlickerOnFraction);
				}
				else
				{
					SetVisible(true);
					flickerToggleTime = Time.time + lowHealthFlickerPeriod * lowHealthFlickerOnFraction;
				}
			}
		}
	}

	public void UpdateHealth(int percentage)
	{
		healthPercentage = percentage;
		var alphaKeys = healthPercentage > 0 ? mainParticles.colorOverLifetime.color.gradient.alphaKeys : invisibleAlphaKeys;
		if (mainParticles != null)
		{
			UpdateShieldColor(GetColorKeysForCurrentHealth(), alphaKeys);
		}
		Debug.Log("Current health: " + healthPercentage + "%");
	}

	void UpdateShieldColor(GradientColorKey[] colorKeys, GradientAlphaKey[] alphaKeys)
	{
		Gradient grad = new Gradient();
		grad.SetKeys(colorKeys, alphaKeys);
		var colorOverLifetimeModule = mainParticles.colorOverLifetime;
		colorOverLifetimeModule.color = grad;
	}

	private GradientColorKey[] GetColorKeysForCurrentHealth()
	{
		int colorBandIndex = totalColorBands * (healthPercentage - 1) / 100; // the minus one makes the bands including upper limit and excluding lower limit. E.g.: for 5 bands, 80% will map to index 3, not 4.
		if (colorBandIndex > totalColorBands - 1)
		{
			Debug.LogWarning("Color band index greater than array size. Clamping.");
			colorBandIndex = totalColorBands - 1;
		}
		return colorBands[colorBandIndex];
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag.Equals(Constants.ENEMY_WEAPON))
		{
			var healthPercentageBeforeAttack = healthPercentage;
			other.transform.root.gameObject.SendMessage(Constants.TARGET_TRIGGER_HIT);
			if (healthPercentageBeforeAttack > 0)
			{
				PlayImpactParticles(other);
				hitSound.Play(audioSource);
			}
		}
	}

	private void PlayImpactParticles(Collider other)
	{
		var impactParticles = LeanPool.Spawn(impactParticlesPrefab, gameObject.transform.position, gameObject.transform.rotation, gameObject.transform);
		if (impactParticles)
		{
			var impactMainModule = impactParticles.main;
			impactMainModule.maxParticles = maxSparks;
			var emission = impactParticles.emission;
			emission.SetBursts(new ParticleSystem.Burst[]
				{new ParticleSystem.Burst(0f, (short) (0.8f * maxSparks), (short) maxSparks)});
			var contactPoint = other.transform.position;
			impactParticles.transform.position = contactPoint;
			impactParticles.transform.LookAt(contactPoint - transform.position);
			var color = GetColorKeysForCurrentHealth()[0].color;
			impactMainModule.startColor = color;
			impactParticles.Play();
			LeanPool.Despawn(impactParticles, impactMainModule.startLifetime.constant);
		}
	}
}
