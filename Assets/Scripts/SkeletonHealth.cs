﻿public class SkeletonHealth : Health
{
    private Skeleton skeleton;

    private void Start()
    {
        skeleton = GetComponent<Skeleton>();
    }

    public override void OnHealthUpdate()
    {
        // do nothing
    }

    public override void OnZeroHealth()
    {
        if (skeleton != null)
        {
            skeleton.SendMessage(Constants.IS_DEAD);
        }
        
    }
}
