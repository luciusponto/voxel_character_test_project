﻿using UnityEngine;

[RequireComponent(typeof(Light))]
public class FlickeringLight : MonoBehaviour
{

	public float MinIntensity;
	public float MaxIntensity;
	public float MinLength;
	public float MaxLength;
	public AnimationCurve IntensityProbability;
	public AnimationCurve LengthProbability;
	private bool currentlyFlickering;
	private float initialIntensity;
	private float targetIntensity;
	private float length;
	private Light light;
	public float intensityMultiplier = 1;
	private float t;
	
	// Use this for initialization
	void Start ()
	{
		light = GetComponent<Light>();
	}
	
	// Update is called once per frame
	void Update () {
		if (!currentlyFlickering)
		{
			targetIntensity = (MinIntensity + IntensityProbability.Evaluate(Random.Range(0f, 1f)) * (MaxIntensity - MinIntensity)) * intensityMultiplier;
			length = MinLength + LengthProbability.Evaluate(Random.Range(0f, 1f)) * (MaxLength - MinLength);
			initialIntensity = light.intensity;
			t = 0;
			currentlyFlickering = true;
		}
		else
		{
			t += length * Time.deltaTime;
			light.intensity = Mathf.Lerp(initialIntensity, targetIntensity, t);
			if (t >= 1)
			{
				currentlyFlickering = false;
			}
		}
		
	}
}
