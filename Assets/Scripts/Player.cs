﻿using UnityEngine;

public class Player : MonoBehaviour {
	
	private void Start()
	{
		GameManager.GetInstance().AddPlayer(gameObject);
	}

	private void OnDestroy()
	{
		GameManager.GetInstance().RemovePlayer(gameObject);
	}
}
