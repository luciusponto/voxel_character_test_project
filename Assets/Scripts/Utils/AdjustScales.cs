﻿using UnityEngine;

public class AdjustScales : MonoBehaviour
{
	public float hair = 1;
	public float hat = 1;
	public float beard = 1;
	public float cape = 1;
	
	public void Adjust()
	{
		Adjust("Hair17x4Vx", hair);
		Adjust("Beard17x4Vx", beard);
		Adjust("HeadGear17x4Vx", hat);
		Adjust("Cape17x4VxModular", cape);
	}

	private void Adjust(string name, float value)
	{
		var objectToAdjust = FindDeepChild(gameObject.transform, name);
		if (objectToAdjust != null)
		{
			objectToAdjust.transform.localScale = Vector3.one * value;
		}
	}
	
	public static Transform FindDeepChild(Transform parent, string name)
	{
		var result = parent.Find(name);
		if (result != null)
			return result;
		foreach(Transform child in parent)
		{
			result = FindDeepChild(child, name);
			if (result != null)
				return result;
		}
		return null;
	}
}
